import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
	selector: 'page-about',
	templateUrl: 'about.html'
})
export class AboutPage {

	item;
	aliados;
	
	constructor(public navCtrl: NavController, public params: NavParams) {
		this.item = params.data.item;
		this.aliados = params.data.item.biography['aliases'];
		console.log(this.item);
		console.log(this.item.connections);
	}

}
