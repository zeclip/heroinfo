	import { Component } from '@angular/core';
	import { NavController } from 'ionic-angular';
	import { Proveedor1Provider } from '../../providers/proveedor1/proveedor1';
	import { AboutPage } from '../about/about';

	@Component({
		selector: 'page-home',
		templateUrl: 'home.html'
	})
	export class HomePage {

		usuarios = {};
		valor = 1;

		constructor(public navCtrl: NavController, public provedor: Proveedor1Provider) {}

		getItems(ev) {
			var val = ev.target.value;
			if(val.length > 1){
				this.provedor.obtenerDatos(val)
				.subscribe(
					(data)=> {this.usuarios = data,this.valor=0},
					(error)=> {alert(error);}
					)
			}else{
				this.valor=1;
			}
		}
		openNavDetailsPage(item) {
			this.navCtrl.push(AboutPage, { item: item });
		}
	}